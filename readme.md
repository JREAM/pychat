# Windows
---

### Create
    $ mkdir project
    $ cd project
    $ virtualenv env
    $ source env/Scripts/activate
    $ pip install -r requirements.txt
    $ cd project
    $ python chat.py

    
### Run
Open chat.html in browser