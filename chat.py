# Imports
import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado.websocket

PORT = 8888
CLIENTS = []

class Websocket(tornado.websocket.WebSocketHandler):

    def open(self):
        CLIENTS.append(self)

    def on_message(self, message):
        for c in CLIENTS:
            c.write_message(u"{0}".format(message))

    def on_close(self):
        CLIENTS.remove(self)


application= tornado.web.Application([
    (r'/websocket$', Websocket),
])

http_server = tornado.httpserver.HTTPServer(application)
http_server.listen(PORT)
tornado.ioloop.IOLoop.instance().start()